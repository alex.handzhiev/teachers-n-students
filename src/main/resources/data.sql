DROP TABLE IF EXISTS person_course;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS person;

CREATE TABLE course (
  course_id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(160),
  course_type VARCHAR(10)
);

CREATE TABLE person (
  person_id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(160),
  class_group VARCHAR(160),
  age int,
  person_type VARCHAR(10),
  course_id int,
  FOREIGN KEY (course_id) REFERENCES course (course_id)
);