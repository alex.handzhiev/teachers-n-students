package app.dao;

import app.model.Course;
import app.model.CourseType;
import app.model.PersonType;
import app.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoursesRepository extends CrudRepository<Course, Long> {

    Iterable<Course> findAllByType(CourseType type);

    int countByType(CourseType type);
}