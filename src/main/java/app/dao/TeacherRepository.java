package app.dao;

import app.model.CourseType;
import app.model.PersonType;
import app.model.Teacher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends CrudRepository<Teacher, Long> {

    Iterable<Teacher> findAllByType(PersonType type);

    int countByType(PersonType type);
}