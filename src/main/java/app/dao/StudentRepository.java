package app.dao;

import app.model.Course;
import app.model.CourseType;
import app.model.PersonType;
import app.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    Iterable<Student> findAllByType(PersonType type);

    int countByType(PersonType type);

    List<Student> findAllStudentsByGroup(String group);
    List<Student> findStudentsByCourseName(String courseName);
    List<Student> findStudentsByCourseNameAndAgeGreaterThan(String coursName, int age);
}