package app.controller;

import app.dao.CoursesRepository;
import app.dao.StudentRepository;
import app.model.Course;
import app.model.CourseType;
import app.model.PersonType;
import app.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/courses")
public class CoursesController {

    @Autowired
    CoursesRepository coursesRepository;

    @GetMapping()
    public ResponseEntity allCourses() {
        return new ResponseEntity(coursesRepository.findAll(), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity create(@RequestBody Course course) {
        Course created = coursesRepository.save(course);
        return new ResponseEntity(created, new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping()
    public ResponseEntity updateStudents(@RequestBody Course course) {
        Course updated = coursesRepository.save(course);
        return new ResponseEntity(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/count/{type}")
    public ResponseEntity countCourses(@PathVariable CourseType type) {
        return new ResponseEntity("Courses: " + coursesRepository.countByType(type),
                new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteCourse(@PathVariable Long id) {
        coursesRepository.deleteById(id);
    }

}
