package app.controller;

import app.dao.CoursesRepository;
import app.dao.TeacherRepository;
import app.model.Course;
import app.model.PersonType;
import app.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    CoursesRepository coursesRepository;

    @GetMapping("/{id}")
    public ResponseEntity findTeacherById(@PathVariable Long id) {
        return new ResponseEntity(teacherRepository.findById(id), new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity allTeachers() {
        return new ResponseEntity(teacherRepository.findAllByType(PersonType.TEACHER), new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteTeacher(@PathVariable Long id) {
        teacherRepository.deleteById(id);
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity create(@RequestBody Teacher teacher) {
        if(teacher.getCourse() != null) {
            Course course = coursesRepository.save(teacher.getCourse());
            teacher.setCourse(course);
        }
        Teacher created = teacherRepository.save(teacher);
        return new ResponseEntity(created, new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping()
    @ResponseBody
    public ResponseEntity updateStudents(@RequestBody Teacher teacher) {
        Teacher updated = teacherRepository.save(teacher);
        return new ResponseEntity(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/count")
    @ResponseBody
    public ResponseEntity countTeachers() {
        return new ResponseEntity("Teachers: " + teacherRepository.countByType(PersonType.TEACHER),
            new HttpHeaders(), HttpStatus.OK);
    }
}
