package app.controller;

import app.dao.CoursesRepository;
import app.dao.StudentRepository;
import app.model.Course;
import app.model.PersonType;
import app.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    CoursesRepository coursesRepository;

    @GetMapping()
    public ResponseEntity allStudents() {
        return new ResponseEntity(studentRepository.findAllByType(PersonType.STUDENT), new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping()
    @ResponseBody
    public ResponseEntity create(@RequestBody Student student) {
        if(student.getCourse() != null) {
            Course course = coursesRepository.save(student.getCourse());
            student.setCourse(course);
        }
        Student created = studentRepository.save(student);
        return new ResponseEntity(created, new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping()
    @ResponseBody
    public ResponseEntity updateStudents(@RequestBody Student student) {
        Student updated = studentRepository.save(student);
        return new ResponseEntity(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/count")
    @ResponseBody
    public ResponseEntity countStudents() {
        return new ResponseEntity("Students: " + studentRepository.countByType(PersonType.STUDENT),
                new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/filter")
    public ResponseEntity findStudents(@RequestParam(required = false) String group,
                                       @RequestParam(required = false) String course,
                                       @RequestParam(required = false) Integer age) {
        List<Student> result = new ArrayList<>();
        if(!StringUtils.isEmpty(group)) {
            result = studentRepository.findAllStudentsByGroup(group);
        } else if (!StringUtils.isEmpty(course) && age != null) {
            result = studentRepository.findStudentsByCourseNameAndAgeGreaterThan(course, age.intValue());
        } else if (!StringUtils.isEmpty(course) && age == null) {
            result = studentRepository.findStudentsByCourseName(course);
        }
        return new ResponseEntity(result, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    void deleteStudent(@PathVariable Long id) {
        studentRepository.deleteById(id);
    }

}
