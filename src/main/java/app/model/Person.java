package app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person")
@MappedSuperclass
public class Person implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int person_id;
    @Column(name = "name")
    String name;
    @Column(name = "class_group")
    String group;
    @Column(name = "age")
    Integer age;
    @Column(name = "personType")
    @Enumerated(EnumType.STRING)
    PersonType type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="course_id")
    Course course;

    public Person(String name, String group, Integer age, PersonType type) {
        this.name = name;
        this.group = group;
        this.age = age;
        this.type = type;
    }
}
