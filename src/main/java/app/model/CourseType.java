package app.model;

public enum CourseType {
    MAIN,
    SECONDARY;
}
