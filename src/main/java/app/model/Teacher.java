package app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "person")
public class Teacher extends Person implements Serializable {

    public Teacher(String name, String group, Integer age) {
        super(name, group, age, PersonType.TEACHER);
    }
}
