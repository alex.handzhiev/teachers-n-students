package app.model;

public enum PersonType {
    STUDENT,
    TEACHER;
}
